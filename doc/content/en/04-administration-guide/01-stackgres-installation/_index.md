---
title: StackGres Installation
weight: 1
url: install
aliases: [ /administration/install ]
description: Documentation how to install the StackGres operator.
---

Documentation how to install the StackGres operator.

{{% children style="li" depth="1" description="true" %}}
